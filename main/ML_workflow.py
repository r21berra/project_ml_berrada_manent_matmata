import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
# Models import list
from sklearn.model_selection import cross_val_score
from sklearn import svm
from sklearn.linear_model import SGDClassifier 
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression


# # 1. Import the dataset
def import_data():

    ##Banknote authentification dataset
    vars_bank=["variance of Wavelet Transformed image (continuous) ","skewness of Wavelet Transformed image (continuous)","curtosis of Wavelet Transformed image (continuous)","entropy of image (continuous)","class (integer)"]
    banknote_data = pd.read_csv("../Datasets/data_banknote_authentication.txt", sep=',', header=None, names=vars_bank)

    ##Kidney disease dataset

    kidney_data= pd.read_csv("../Datasets/kidney_disease.csv", sep=',')
    vars_kidney=kidney_data.columns
    return banknote_data,kidney_data



# # 2. Clean the data, perform pre-processing


def preprocessing(dataset):
    ##kidney's some columns case 
    if ("id" in dataset.columns):
        #pc rbc and appet case  

        label_mapping = {'normal': 1.0, 'abnormal': 0.0}

        dataset["pc"] = dataset["pc"].map(label_mapping)
        dataset["rbc"] = dataset["rbc"].map(label_mapping)
        label_mapping2 = {'good': 1, 'poor': 0}
        dataset["appet"] = dataset["appet"].map(label_mapping2)
        
        mean_pc=(dataset["pc"]).mean()
        mean_rbc=(dataset["pc"]).mean()
        mean_ap=(dataset["appet"]).mean()
        
        dataset["pc"] = dataset["pc"].fillna(round(mean_pc))
        dataset["rbc"] = dataset["rbc"].fillna(round(mean_rbc))
        dataset["appet"] = dataset["appet"].fillna(round(mean_ap))




    # # I Replace binary values by 0 and 1
    # Replace nan values in binary by the rounded mean to the int  

    def replace_str(dataset):
        for i in dataset.columns:#drop("id",axis=1).
            #replace binary variables
            typo_indices=[]
            mask = (dataset[i].astype(str).str.contains('no'))

            if((dataset[i].dtype)!="float64" and mask.any() ):
                has_nan=dataset[i].isna()
                dataset[i]=mask.astype(int)
                if (has_nan.any()):
                    # Then we put the closest int to the mean in nan values
                    mean_d=dataset[i].mean()
                    dataset.loc[has_nan,i]=round(mean_d)
                #print(dataset[i])
                dataset[i].astype("float64")
                #typo_indices = kidney_data[mask].index.tolist()
        
            # if typo_indices:
            #     typos[i] = typo_indices
        return dataset


    ## Replace missing data because of tipos and replace mean values, setting all values type to float (easy to manipulate)
    def replace_missing(dataset):
    #Replace tipos containing \t or ' ' or '?'
    # Replace NaN values with mean
        for col in dataset:
            dataset[col] = dataset[col].replace('\t', '', regex=True)
            dataset[col] = dataset[col].replace(' ', '', regex=False)
            dataset[col] = dataset[col].replace('?', '', regex=False)
            dataset[col] = dataset[col].replace('', 'nan', regex=False)
            dataset[col]= dataset[col].astype(float)
            mean_col=dataset[col].mean()
            dataset[col] = dataset[col].fillna(mean_col)
        
        return dataset


# # I Center and normalize the processed data
    
    dataset =replace_missing(replace_str(dataset))

    return dataset

# # 3. Feature selection using correlation matrix and plotting it 
def feature_selection(data):
    X=data.drop(columns=data.columns[-1] )#remove label one
    data_as_array=X.values
    correlation = abs(np.corrcoef(data_as_array,rowvar=False,))
    f = plt.figure(figsize=(19, 15))
    plt.matshow(correlation, fignum=f.number)
    plt.xticks(range(X.select_dtypes(['number']).shape[1]), X.select_dtypes(['number']).columns, fontsize=14, rotation=45)
    plt.yticks(range(X.select_dtypes(['number']).shape[1]), X.select_dtypes(['number']).columns, fontsize=14)
    cb = plt.colorbar()
    cb.ax.tick_params(labelsize=14)
    plt.title('Correlation Matrix', fontsize=16)

    correlated_values=np.where(np.logical_and(correlation>0.70, correlation<1))
    correlated_features=list(set([X.columns[i] for i in correlated_values[0]]))
    return correlated_features

# # 4. Split the dataset
def split_datasets(dataset, correlated_features,test_size=0.2, random_state=42, n_splits_cv=5):
    # Splitting the dataset into training set and test set
    
    y=dataset[dataset.columns[-1]]
    data_list=np.transpose([dataset[col] for col in correlated_features])
    print(np.shape(data_list))
    X=pd.DataFrame(data_list)
    X= StandardScaler().fit_transform(X) #normalize our data


    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_state)
    # test_size=0.2 means 80% training data, 20% test data (adjust as needed)
    # random_state=None (or any integer) for reproducibility

    

    return (X_train, X_test, y_train, y_test)


# # 5. Tested models 
def set_models():
    models = {svm.SVC(kernel='linear'):'SVM with linear kernel',
            svm.SVC(kernel='poly', degree=2, gamma='auto'):'SVM with polynomial (degree 2) kernel',
            svm.SVC(kernel='rbf', gamma='auto'):'SVM with RBF kernel',
            svm.SVC(kernel='sigmoid', gamma=1./150):'SVM with sigmoid kernel',
            SGDClassifier():'Stochastic Gradient Descent',
            DecisionTreeClassifier():'Decision Trees',
            GaussianNB():'Bayesian classifier',
            RandomForestClassifier(n_estimators = 100, random_state = 42):'Random Forest',
            MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5,2), random_state=42,max_iter=1000):'Neural Network',
            LogisticRegression(random_state=0):'Logistic Regression'}
    return models
    

    
# # 6. Training Models  Scoring the models 
def training_scoring(X,Y,models): 
    
    scores=[]
    final_scores={}
    for clf,value in list(models.items()):
        
        cur_score = (np.mean(cross_val_score(clf, X, Y, cv=10)))
        final_scores[value]=cur_score
        scores.append(cur_score)
    print(list(final_scores.keys())[scores.index(max(scores))],max(scores))
    print(final_scores)
    return final_scores

# # 7.  Plot the cross validation comparision for decision
def plot_result(result):
    labels = list(result.keys())
    values = list(result.values())
    plt.figure(figsize=(10, 6))
    plt.bar(labels, values, color='skyblue')
    plt.scatter(labels,values)
    plt.xlabel('Models')
    plt.ylim(min(values), 1.0) 
    plt.xticks(rotation=90)  
    plt.tight_layout()
    plt.show()
    return plt.show()


