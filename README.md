# Project_ML_BERRADA_MANENT_MATMATA



## Name
Machine Learning Project for IMT Atlantique MCE 2023

## Description
The purpose of the project is to apply a Machine Learning model for Binary classification onto two different datasets:
I Banknote Authentication Dataset: https://archive.ics.uci.edu/ml/datasets/banknote+authentication
This dataset is composed by 1372 instances, with 5 attributes each, and it gives as an output if the Banknote is falsified or not. The purpose will be to train our classification model, to be able to determine if a given banknote is valid or not.

I Chronic Kidney Disease: https://www.kaggle.com/mansoordaku/ckdisease
This dataset is composed by 400 instances, with 24 attributes each, such as patient's age or red blood cell count and gives as an output if a given patient is suffering from chronic kidney disease or not. We will train our classification model to predict if a new patient is likely to suffer from chronic kidney disease.

## Good programming practices

Concerning good programming practices,we focused of these main aspects of programming : 
Collaborative work : we started by implementing our gitlab repository and tried to work on separate branches, one for each student. However, we realised it would be more beneficial for us to consider a branch as a feature, that would be completed, commited , pushed and then merged to the main branch. After being merged, the branched is deleted. 

Debugging : 
This project required a lot of patience concerning the data manipulated. Indeed, the notebook format allowed us to test functions throughout the workflow and show resulting data. The final functions would be then written on ML_workflow in order to be used in ML_run

Comments and being concise : 
We tried to go strictly to the main purpose of the project , find the best Machine learning model that predicts the dataset. However, we didn't omit summarizing the data we have been given , as we consider that it is a first essential step to every public machine learning project. Finally, we commented the different steps and tried to point out comprehensive variable names for every potential contributor.
## Workflow 

### Preprocessing

Using Pandas library, we were able to highlight a few defects the first database had , such as tipos or missing values we tried to correct. The preprocessing function works for both datasets but has an additional treatment for the kidney disease case in order to replace "normal" or "good" with 1 and the opposite with 0.  Overall, preprocessing was : 
- Replacing binary cases with binary values
- Detecting and correcting tipos
- Filling all non numerical cases with the mean of the column



### Feature choice

The feature choice has been made thanks to the correlation matrix. Indeed, we selected the features that had more than 70% correlation score with another value and used them all to train the model 

### Chosen Models 

We chose to use the scikit learn library to implement our classification models. We will implement several models such as :
SVM and Kernel methods : We will use several kernel such as linear kernel, polynomail kernel, Radial basis function kernel, sigmoid
Stochastic Gradient Descent
Decision Trees
Bayesien classifcation
Random Forest
Neural network : We have a neural network composed of 2 layers. One with 5 neurons and the other with 2 neurons.
Logistic regression : Logisitic regression allows to make a classification with an optimization of the "delta" parameter.

### Evaluation and conclusion  


We have performed multiple models and compared them all for one training set / validation set combination for both datasets. We notice that the kidney disease dataset gives better results and even a 100% accuracy for logistic regression. However, the number of features used to train the model can be high ( 12 ), for a bigger number of data ( more than 400 samples ), it could be expensive computationely speaking. Therefore, a principal component analysis might be interesting to reduce the dimensionality. 
However,  the banknote authentification dataset applied on our models gives various interesting results. Indeed, in both cases, the bayesian classifier is the less accurate, but then the Random Forest classifier gives the best score for banknote authentification, which raises the question about how much a "random" based model is able to predict whether a banknote is authentic or note. Therefore, further investigation is required to find an ethically more reliable model.